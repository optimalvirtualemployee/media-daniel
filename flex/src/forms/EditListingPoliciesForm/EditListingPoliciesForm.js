import React from 'react';
import { bool, func, shape, string } from 'prop-types';
import { compose } from 'redux';
import { Form as FinalForm } from 'react-final-form';
import { intlShape, injectIntl, FormattedMessage } from '../../util/reactIntl';
import arrayMutators from 'final-form-arrays';
import classNames from 'classnames';
import { propTypes } from '../../util/types';
import { Button, FieldCheckboxGroup, FieldTextInput, Form} from '../../components';
import config from '../../config';
import css from './EditListingPoliciesForm.css';

export const EditListingPoliciesFormComponent = props => (
  <FinalForm
    {...props}
    mutators={{ ...arrayMutators }}
    render={formRenderProps => {
      const {
        className,
        disabled,
        ready,
        handleSubmit,
        intl,
        invalid,
        pristine,
        saveActionMsg,
        updated,
        updateInProgress,
        fetchErrors,
      } = formRenderProps;

      const rulesLabelMessage = intl.formatMessage({
        id: 'EditListingPoliciesForm.rulesLabel',
      });
      const rulesPlaceholderMessage = intl.formatMessage({
        id: 'EditListingPoliciesForm.rulesPlaceholder',
      });
      const accessOtherLabelMessage = intl.formatMessage({
        id: 'EditListingPoliciesForm.accessOtherLabel',
      });
      const accessOtherPlaceholderMessage = intl.formatMessage({
        id: 'EditListingPoliciesForm.accessOtherPlaceholder',
      });
            const accessSpecialLabelMessage = intl.formatMessage({
        id: 'EditListingPoliciesForm.accessSpecialLabel',
      });
      const accessSpecialPlaceholderMessage = intl.formatMessage({
        id: 'EditListingPoliciesForm.accessSpecialPlaceholder',
      });



      const { updateListingError, showListingsError } = fetchErrors || {};
      const errorMessage = updateListingError ? (
        <p className={css.error}>
          <FormattedMessage id="EditListingPoliciesForm.updateFailed" />
        </p>
      ) : null;
      const errorMessageShowListing = showListingsError ? (
        <p className={css.error}>
          <FormattedMessage id="EditListingPoliciesForm.showListingFailed" />
        </p>
      ) : null;

      const classes = classNames(css.root, className);
      const submitReady = (updated && pristine) || ready;
      const submitInProgress = updateInProgress;
      const submitDisabled = invalid || disabled || submitInProgress;

      return (
        <Form className={classes} onSubmit={handleSubmit}>
          {errorMessage}
          {errorMessageShowListing}

        <p>This property is accessed via:</p>
          <FieldCheckboxGroup
            className={css.features}
            id="access"
            name="access"
            options={config.custom.access}
          />
            <FieldTextInput
            id="accessOther"
            name="accessOther"
            className={css.policy}
            type="textarea"
            label={accessOtherLabelMessage}
            placeholder={accessOtherPlaceholderMessage}
          />

          <FieldCheckboxGroup
            className={css.featuresaccess}
            id="accessSpecial"
            name="accessSpecial"
            options={config.custom.accessspecial}
          />

          <FieldTextInput
            id="accessSpecialText"
            name="accessSpecialText"
            className={css.policy}
            type="textarea"
            label={accessSpecialLabelMessage}
            placeholder={accessSpecialPlaceholderMessage}
          />



  <h1><FormattedMessage id="EditListingPoliciesPanel.createListingRules" /></h1>
          <FieldTextInput
            id="rules"
            name="rules"
            className={css.policy}
            type="textarea"
            label={rulesLabelMessage}
            placeholder={rulesPlaceholderMessage}
          />








          <Button
            className={css.submitButton}
            type="submit"
            inProgress={submitInProgress}
            disabled={submitDisabled}
            ready={submitReady}
          >
            {saveActionMsg}
          </Button>
        </Form>
      );
    }}
  />
);

EditListingPoliciesFormComponent.defaultProps = {
  selectedPlace: null,
  updateError: null,
};

EditListingPoliciesFormComponent.propTypes = {
  intl: intlShape.isRequired,
  onSubmit: func.isRequired,
  saveActionMsg: string.isRequired,
  selectedPlace: propTypes.place,
  disabled: bool.isRequired,
  ready: bool.isRequired,
  updated: bool.isRequired,
  updateInProgress: bool.isRequired,
  fetchErrors: shape({
    showListingsError: propTypes.error,
    updateListingError: propTypes.error,
  }),
};

export default compose(injectIntl)(EditListingPoliciesFormComponent);
