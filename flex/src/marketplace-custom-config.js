/*
 * Marketplace specific configuration.
 */

export const amenities = [
  {
    key: 'toilet',
    label: 'Toilet',
  },
  {
    key: 'shower',
    label: 'Shower',
  },
  {
    key: 'bbq',
    label: 'BBQ',
  },
  {
    key: 'wifi',
    label: 'WIFI',
  },
  {
    key: 'kitchen',
    label: 'Kitchen',
  },
  {
    key: 'water',
    label: 'Water',
  },
  {
    key: 'pets',
    label: 'Pets',
  }
];

export const land_features = [
  {
    key: 'beach',
    label: 'Beach',
  },
  {
    key: 'bush',
    label: 'Bush',
  },
  {
    key: 'forrest',
    label: 'Forrest',
  },
  {
    key: 'creek',
    label: 'Creek',
  },
  {
    key: 'lakes',
    label: 'Lakes',
  },
  {
    key: 'dams',
    label: 'Dams',
  },
  {
    key: 'river',
    label: 'River',
  },
  {
    key: 'desert',
    label: 'Desert',
  }
];

export const activities = [
  {
    key: 'swimming',
    label: 'Swimming',
  },
  {
    key: 'kyaking',
    label: 'Kyaking',
  },
  {
    key: 'fishing',
    label: 'Fishing',
  },
  {
    key: 'bike_Riding',
    label: 'Bike riding',
  },
  {
    key: 'horse_riding',
    label: 'Horse Riding',
  },
  {
    key: 'climbing',
    label: 'Climbing',
  },
  {
    key: '4wd',
    label: '4wd',
  },
  {
    key: 'surfing',
    label: 'Surfing',
  },
  {
    key: 'paddle_boarding',
    label: 'Paddle Borading',
  }
];



export const access = [
  {
    key: 'sealed_road',
    label: 'Sealed road',
  },
  {
    key: 'unsealed_road',
    label: 'Unsealed road',
  },
  {
    key: '4wd_required',
    label: '4wd required',
  },
  {
    key: 'walking_track',
    label: 'Walking Track',
  }
];


export const accessspecial = [
  {
    key: 'mobility',
    label: 'This property has access and mobility for people with wheelchairs',
  },
  {
    key: 'guide_dogs',
    label: 'This property has access for guide dogs',
  }
];


export const categories = [
  { key: 'powered', label: 'Powered' },
  { key: 'unpowered', label: 'Unpowered' },
];

export const size = [
  { key: 'small', label: 'Small (5m x 5m)' },
  { key: 'medium', label: 'Medium (10m x10m)' },
  { key: 'large', label: 'Large (100m2+)' },
];

export const people = [
  { key: 'one', label: '1' },
  { key: 'two', label: '2' },
  { key: 'three', label: '3' },
  { key: 'four', label: '4' },
  { key: 'five', label: '5' },
  { key: 'six', label: '6' },
  { key: 'seven', label: '7' },
  { key: 'eight', label: '8' },
  { key: 'nine', label: '9' },
  { key: 'ten', label: '10' },
];


// Price filter configuration
// Note: unlike most prices this is not handled in subunits
export const priceFilterConfig = {
  min: 0,
  max: 1000,
  step: 5,
};

// Activate booking dates filter on search page
export const dateRangeFilterConfig = {
  active: true,
};

// Activate keyword filter on search page

// NOTE: If you are ordering search results by distance the keyword search can't be used at the same time.
// You can turn off ordering by distance in config.js file
export const keywordFilterConfig = {
  active: true,
};

export const sortConfig = {
  // Enable/disable the sorting control in the SearchPage
  active: true,

  // Internal key for the relevance option, see notes below.
  relevanceKey: 'relevance',

  options: [
    { key: 'createdAt', label: 'Newest' },
    { key: '-createdAt', label: 'Oldest' },
    { key: '-price', label: 'Lowest price' },
    { key: 'price', label: 'Highest price' },

    // The relevance is only used for keyword search, but the
    // parameter isn't sent to the Marketplace API. The key is purely
    // for handling the internal state of the sorting dropdown.
    { key: 'relevance', label: 'Relevance', longLabel: 'Relevance (Keyword search)' },
  ],
};
