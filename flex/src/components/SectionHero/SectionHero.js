import React from 'react';
import { string } from 'prop-types';
import { FormattedMessage } from '../../util/reactIntl';
import classNames from 'classnames';
import { NamedLink } from '../../components';

import css from './SectionHero.css';

const SectionHero = props => {
  const { rootClassName, className } = props;

  const classes = classNames(rootClassName || css.root, className);

  return (
    <div className={classes}>
 <div className={css.vimeowrapper}>
   <iframe title="GoCamp Teaser Video" src="https://player.vimeo.com/video/320072753?background=1&autoplay=1&loop=1&byline=0&title=0"
           frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>
      <div className={css.heroContent}>

        <h1 className={css.heroMainTitle1}>
        <div className={css.linetitle}>
        <svg width="351px" height="13px" viewBox="0 0 421 13" id="lineanimate">
            <title>Bed Line</title>
            <g id="1---Landing-page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                <g class="linebed" id="1.0---Homepage" transform="translate(-513.000000, -222.000000)" stroke="#FFFFFF" stroke-width="4">
                <path d="M515,224 C765,230 904,233 932,233" id="Path-2"></path>
                </g>
            </g>
        </svg>
        </div>
          <FormattedMessage id="SectionHero.title1" />
        </h1>

        <h1 className={css.heroMainTitle2}>
        <div className={css.linetitle}>
        <svg width="259px" height="5px" viewBox="0 0 329 5" id="lineanimate">
            <title>Work Line</title>
              <g class="linework" id="1---Landing-page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
              <g id="1.0---Homepage" transform="translate(-547.000000, -262.000000)" stroke="#FFFFFF" stroke-width="4">
              <path d="M549,264 C739,266 847.333333,266 874,264" id="Path-3"></path>
              </g>
          </g>
        </svg>
        </div>

          <FormattedMessage id="SectionHero.title2" />
        </h1>

        <h1 className={css.heroMainTitle3}>
          <FormattedMessage id="SectionHero.title3" />
        </h1>


        <h2 className={css.heroSubTitle}>
          <FormattedMessage id="SectionHero.subTitle" />
        </h2>
        <NamedLink
          name="SearchPage"
          to={{
            search:
              'address=Australia&bounds=-9.04271800049806%2C159.209167991267%2C-43.8402889914172%2C112.82133800159',
          }}
          className={css.heroButton}
        >
          <FormattedMessage id="SectionHero.browseButton" />
        </NamedLink>
      </div>
    </div>
  );
};

SectionHero.defaultProps = { rootClassName: null, className: null };

SectionHero.propTypes = {
  rootClassName: string,
  className: string,
};

export default SectionHero;
