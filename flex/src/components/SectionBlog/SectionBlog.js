import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from '../../util/reactIntl';
import classNames from 'classnames';
import { lazyLoadWithDimensions } from '../../util/contextHelpers';

import { NamedLink } from '../../components';

import css from './SectionBlog.css';

import australiaOutback from './images/australia_outback.jpg';
import rvSpots from './images/rv_spots.jpg';
import riverSide from './images/river_side.jpg';

class LocationImage extends Component {
  render() {
    const { alt, ...rest } = this.props;
    return <img alt={alt} {...rest} />;
  }
}
const LazyImage = lazyLoadWithDimensions(LocationImage);

const locationLink = (name, image, searchQuery) => {
  const nameText = <span className={css.locationName}>{name}</span>;
  return (
    <NamedLink name="SearchPage" to={{ search: searchQuery }} className={css.location}>
      <div className={css.imageWrapper}>
        <div className={css.aspectWrapper}>
          <LazyImage src={image} alt={name} className={css.locationImage} />
        </div>
      </div>
      <div className={css.linkText}>
        <FormattedMessage
          id="SectionBlog.blogTitle"
          values={{ location: nameText }}
        />
      </div>
    </NamedLink>
  );
};

const SectionBlog = props => {
  const { rootClassName, className } = props;

  const classes = classNames(rootClassName || css.root, className);

  return (
    <div className={classes}>
      <div className={css.title}>
        <FormattedMessage id="SectionBlog.title" />
      </div>
      <div className={css.locations}>
        {locationLink(
          'Ways to explore',
          australiaOutback,
          '?address=Australia&bounds=-9.04271800049806%2C159.209167991267%2C-43.8402889914172%2C112.82133800159'
        )}
        {locationLink(
          'The great outdoors is the great holiday',
          rvSpots,
          '?address=Australia&bounds=-9.04271800049806%2C159.209167991267%2C-43.8402889914172%2C112.82133800159'
        )}
        {locationLink(
          'River Side',
          riverSide,
          '?address=Australia&bounds=-9.04271800049806%2C159.209167991267%2C-43.8402889914172%2C112.82133800159'
        )}
      </div>
    </div>
  );
};

SectionBlog.defaultProps = { rootClassName: null, className: null };

const { string } = PropTypes;

SectionBlog.propTypes = {
  rootClassName: string,
  className: string,
};

export default SectionBlog;
