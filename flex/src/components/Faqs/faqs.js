
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';


function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}class AccordionItem extends React.Component {
  render() {
    const { accordion, onHeaderClick, isOpen } = this.props;
    return (
      React.createElement("div", { className: "accordion__item accordion-part" },
      React.createElement("div", { className: `accordion-part__header ${isOpen ? 'accordion-part__header_opened' : ''}`,
        onClick: onHeaderClick },

      accordion.title),

      isOpen &&
      React.createElement("div", { className: "accordion-part__body" },
      React.createElement("p", { className: "accordion-part__text" },
      accordion.text))));






  }}


class AccordionList extends React.Component {constructor(...args) {super(...args);_defineProperty(this, "state",
    {
      activeAccordion: 0 });_defineProperty(this, "headerClick",


    (index) =>
    this.setState(prev => ({
      activeAccordion: prev.activeAccordion === index ? -1 : index })));}



  render() {
    const accordionElement = this.props.accordions.map((item, index) =>
    React.createElement(AccordionItem, {
      key: item.title,
      accordion: item,
      isOpen: this.state.activeAccordion === index,
      onHeaderClick: this.headerClick.bind(null, index) }));



    return (
      React.createElement("div", { className: "accordion__list" },
      accordionElement));


  }}






const accordions = [
{
  title: 'What is Lorem Ipsum?',
  text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.' },

{
  title: 'Where does it come from?',
  text: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.' },

{
  title: 'Why do we use it?',
  text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.' },

{
  title: 'Where can I get some?',
  text: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.' }];




class App extends React.Component {
  render() {
    return (
      React.createElement("div", { className: "container" },
      React.createElement("div", { className: "accordion" },
      React.createElement(AccordionList, { accordions: accordions }))));



  }}


ReactDOM.render(React.createElement(App, null), document.getElementById('root'));

