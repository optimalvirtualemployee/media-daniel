import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from '../../util/reactIntl';
import classNames from 'classnames';
import imageCamp from './gocamp-icon-camp-static.svg';
import imageHost from './gocamp-icon-host-static.svg';
import image from './outdoor.jpg';


import { NamedLink } from '../../components';

import css from './SectionWhatIsGoCamp.css';

const SectionWhatIsGoCamp = props => {
  const { rootClassName, className } = props;

  const classes = classNames(rootClassName || css.root, className);
  return (
    <div className={classes}>
      <div className={css.title}>
        <FormattedMessage id="SectionWhatIsGoCamp.introTitle" />
        <p className={css.introText}>
        <FormattedMessage id="SectionWhatIsGoCamp.introText" />
        </p>
      </div>

      <div className={css.steps}>
        <div className={css.step}>
          <div className={css.image}>
            <img className={css.imageCamp} src={imageCamp} alt="Camp" />

<svg width="407px" height="11px" viewBox="0 0 407 11">

    <g id="1---Landing-page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <path id="Path-2-Copy" d="M397.745131,0.564212231 L405.745131,5.06421223 L406.519864,5.5 L405.745131,5.93578777 L397.745131,10.4357878 L397.254869,9.56421223 L404.48,5.5 L397.254869,1.43578777 L397.745131,0.564212231 Z M5,1 C7.31637876,1 9.22398135,2.7501788 9.47256844,5.00029704 L15,5 L15,6 L9.47245735,6.00070744 C9.2234136,8.25034205 7.31603406,10 5,10 C2.51471863,10 0.5,7.98528137 0.5,5.5 C0.5,3.01471863 2.51471863,1 5,1 Z M5,2 C3.06700338,2 1.5,3.56700338 1.5,5.5 C1.5,7.43299662 3.06700338,9 5,9 C6.93299662,9 8.5,7.43299662 8.5,5.5 C8.5,3.56700338 6.93299662,2 5,2 Z M27,5 L27,6 L21,6 L21,5 L27,5 Z M39,5 L39,6 L33,6 L33,5 L39,5 Z M51,5 L51,6 L45,6 L45,5 L51,5 Z M63,5 L63,6 L57,6 L57,5 L63,5 Z M75,5 L75,6 L69,6 L69,5 L75,5 Z M87,5 L87,6 L81,6 L81,5 L87,5 Z M99,5 L99,6 L93,6 L93,5 L99,5 Z M111,5 L111,6 L105,6 L105,5 L111,5 Z M123,5 L123,6 L117,6 L117,5 L123,5 Z M135,5 L135,6 L129,6 L129,5 L135,5 Z M147,5 L147,6 L141,6 L141,5 L147,5 Z M159,5 L159,6 L153,6 L153,5 L159,5 Z M171,5 L171,6 L165,6 L165,5 L171,5 Z M183,5 L183,6 L177,6 L177,5 L183,5 Z M195,5 L195,6 L189,6 L189,5 L195,5 Z M207,5 L207,6 L201,6 L201,5 L207,5 Z M219,5 L219,6 L213,6 L213,5 L219,5 Z M231,5 L231,6 L225,6 L225,5 L231,5 Z M243,5 L243,6 L237,6 L237,5 L243,5 Z M255,5 L255,6 L249,6 L249,5 L255,5 Z M267,5 L267,6 L261,6 L261,5 L267,5 Z M279,5 L279,6 L273,6 L273,5 L279,5 Z M291,5 L291,6 L285,6 L285,5 L291,5 Z M303,5 L303,6 L297,6 L297,5 L303,5 Z M315,5 L315,6 L309,6 L309,5 L315,5 Z M327,5 L327,6 L321,6 L321,5 L327,5 Z M339,5 L339,6 L333,6 L333,5 L339,5 Z M351,5 L351,6 L345,6 L345,5 L351,5 Z M363,5 L363,6 L357,6 L357,5 L363,5 Z M375,5 L375,6 L369,6 L369,5 L375,5 Z M387,5 L387,6 L381,6 L381,5 L387,5 Z M399,5 L399,6 L393,6 L393,5 L399,5 Z" fill="#979797" fill-rule="nonzero"></path>
    </g>
</svg>


        </div>
          <h2 className={css.stepTitle}>
            <FormattedMessage id="SectionWhatIsGoCamp.part1Title" />
          </h2>
          <p className={css.stepText}>
            <FormattedMessage id="SectionWhatIsGoCamp.part1Text" />
          </p>
        <NamedLink name="CampPage" className={css.btndark}>
          <FormattedMessage id="SectionWhatIsGoCamp.campLink" />
        </NamedLink>
        </div>

        <div className={css.step}>
          <div className={css.image}>
            <img className={css.imageHost} src={imageHost} alt="Camp" />
            <svg width="407px" height="11px" viewBox="0 0 407 11">
    <g id="1---Landing-page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <path id="Path-2-Copy" d="M397.745131,0.564212231 L405.745131,5.06421223 L406.519864,5.5 L405.745131,5.93578777 L397.745131,10.4357878 L397.254869,9.56421223 L404.48,5.5 L397.254869,1.43578777 L397.745131,0.564212231 Z M5,1 C7.31637876,1 9.22398135,2.7501788 9.47256844,5.00029704 L15,5 L15,6 L9.47245735,6.00070744 C9.2234136,8.25034205 7.31603406,10 5,10 C2.51471863,10 0.5,7.98528137 0.5,5.5 C0.5,3.01471863 2.51471863,1 5,1 Z M5,2 C3.06700338,2 1.5,3.56700338 1.5,5.5 C1.5,7.43299662 3.06700338,9 5,9 C6.93299662,9 8.5,7.43299662 8.5,5.5 C8.5,3.56700338 6.93299662,2 5,2 Z M27,5 L27,6 L21,6 L21,5 L27,5 Z M39,5 L39,6 L33,6 L33,5 L39,5 Z M51,5 L51,6 L45,6 L45,5 L51,5 Z M63,5 L63,6 L57,6 L57,5 L63,5 Z M75,5 L75,6 L69,6 L69,5 L75,5 Z M87,5 L87,6 L81,6 L81,5 L87,5 Z M99,5 L99,6 L93,6 L93,5 L99,5 Z M111,5 L111,6 L105,6 L105,5 L111,5 Z M123,5 L123,6 L117,6 L117,5 L123,5 Z M135,5 L135,6 L129,6 L129,5 L135,5 Z M147,5 L147,6 L141,6 L141,5 L147,5 Z M159,5 L159,6 L153,6 L153,5 L159,5 Z M171,5 L171,6 L165,6 L165,5 L171,5 Z M183,5 L183,6 L177,6 L177,5 L183,5 Z M195,5 L195,6 L189,6 L189,5 L195,5 Z M207,5 L207,6 L201,6 L201,5 L207,5 Z M219,5 L219,6 L213,6 L213,5 L219,5 Z M231,5 L231,6 L225,6 L225,5 L231,5 Z M243,5 L243,6 L237,6 L237,5 L243,5 Z M255,5 L255,6 L249,6 L249,5 L255,5 Z M267,5 L267,6 L261,6 L261,5 L267,5 Z M279,5 L279,6 L273,6 L273,5 L279,5 Z M291,5 L291,6 L285,6 L285,5 L291,5 Z M303,5 L303,6 L297,6 L297,5 L303,5 Z M315,5 L315,6 L309,6 L309,5 L315,5 Z M327,5 L327,6 L321,6 L321,5 L327,5 Z M339,5 L339,6 L333,6 L333,5 L339,5 Z M351,5 L351,6 L345,6 L345,5 L351,5 Z M363,5 L363,6 L357,6 L357,5 L363,5 Z M375,5 L375,6 L369,6 L369,5 L375,5 Z M387,5 L387,6 L381,6 L381,5 L387,5 Z M399,5 L399,6 L393,6 L393,5 L399,5 Z" fill="#979797" fill-rule="nonzero"></path>
    </g>
</svg>
          </div>
          <h2 className={css.stepTitle}>
            <FormattedMessage id="SectionWhatIsGoCamp.part2Title" />
          </h2>
          <p className={css.stepText}>
            <FormattedMessage id="SectionWhatIsGoCamp.part2Text" />
          </p>
        <NamedLink name="HostPage" className={css.btndark}>
          <FormattedMessage id="SectionWhatIsGoCamp.hostLink" />
        </NamedLink>
        </div>



      </div>



              <div class={css.sectioncardhalf}>
                    <img className={css.sectioncardimagehalf} src={image} alt="Camp" />
      <div class={css.sectioncardtext}>
          <h2 class={css.cardtitlehalf}> THE GREAT OUTDOORS IS <br />THE GREATEST HOLIDAY</h2>

                <p class={css.cardtexthalf}>We’ve sussed out the best secret spots on private<br />
                        lAustralia is a big, wide and beautiful country. There are still backroads to explore, new hills
        to climb, more streams to swim and unique campsites to find.
                        </p>
                        <p class={css.cardtexthalf}>For those that want it, its out there. If you wanna go, go.
        </p>
        <NamedLink name="CampPage" className={css.btndark}>
          <FormattedMessage id="SectionWhatIsGoCamp.campLink" />
        </NamedLink>

      </div>

      </div>



          <div className={css.contentWrapper}>
            
            <div className={css.contentMain}>


            </div>
          </div>



    </div>





  );
};

SectionWhatIsGoCamp.defaultProps = { rootClassName: null, className: null };

const { string } = PropTypes;

SectionWhatIsGoCamp.propTypes = {
  rootClassName: string,
  className: string,
};

export default SectionWhatIsGoCamp;
