import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FormattedMessage } from '../../util/reactIntl';

import { LISTING_STATE_DRAFT } from '../../util/types';
import { ensureListing } from '../../util/data';
import { EditListingFeaturesForm } from '../../forms';
import { ListingLink } from '../../components';

import css from './EditListingFeaturesPanel.css';

const FEATURES_NAME = 'land_features';
const AMENITIES_NAME = 'amenities';
const ACTIVITIES_NAME = 'activities';
const ADDITIONALFEATURES_NAME = 'additionalfeatures';
const ADDITIONALAMENITIES = 'additionalamenities';
const ADDITIONALACTIVITIES_NAME = 'additionalactivities';

const EditListingFeaturesPanel = props => {
  const {
    rootClassName,
    className,
    listing,
    disabled,
    ready,
    onSubmit,
    onChange,
    submitButtonText,
    panelUpdated,
    updateInProgress,
    errors,
  } = props;

  const classes = classNames(rootClassName || css.root, className);
  const currentListing = ensureListing(listing);
  const { publicData } = currentListing.attributes;

  const isPublished = currentListing.id && currentListing.attributes.state !== LISTING_STATE_DRAFT;
  const panelTitle = isPublished ? (
    <FormattedMessage
      id="EditListingFeaturesPanel.title"
      values={{ listingTitle: <ListingLink listing={listing} /> }}
    />
  ) : (
    <FormattedMessage id="EditListingFeaturesPanel.createListingTitle" />
  );

  const amenities = publicData && publicData.amenities;
  const land_features = publicData && publicData.land_features;
  const activities = publicData && publicData.activities;
  const additionalfeatures = publicData && publicData.additionalfeatures;
  const additionalamenities = publicData && publicData.additionalamenities;
  const additionalactivities = publicData && publicData.additionalactivities;

  const initialValues = { land_features, amenities, activities, additionalfeatures, additionalamenities, additionalactivities };

  return (
    <div className={classes}>
      <h1 className={css.title}>{panelTitle}</h1>
      <EditListingFeaturesForm
        className={css.form}
        features={FEATURES_NAME}
        amenities={AMENITIES_NAME}
        activities={ACTIVITIES_NAME}
        additionalfeatures={ADDITIONALFEATURES_NAME}
        additionalamenities={ADDITIONALAMENITIES}
        additionalactivities={ADDITIONALACTIVITIES_NAME}


        initialValues={initialValues}
        onSubmit={values => {
          const { amenities = [] } = values;
          const { land_features = [] } = values;
          const { activities = [] } = values;
          const { additionalfeatures, additionalamenities, additionalactivities } = values;

          const updatedValues = {
            publicData: { amenities, land_features, activities, additionalfeatures, additionalamenities, additionalactivities },            
          };

          onSubmit(updatedValues);
        }}
        onChange={onChange}
        saveActionMsg={submitButtonText}
        disabled={disabled}
        ready={ready}
        updated={panelUpdated}
        updateInProgress={updateInProgress}
        fetchErrors={errors}
      />
    </div>
  );
};

EditListingFeaturesPanel.defaultProps = {
  rootClassName: null,
  className: null,
  listing: null,
};

const { bool, func, object, string } = PropTypes;

EditListingFeaturesPanel.propTypes = {
  rootClassName: string,
  className: string,

  // We cannot use propTypes.listing since the listing might be a draft.
  listing: object,

  disabled: bool.isRequired,
  ready: bool.isRequired,
  onSubmit: func.isRequired,
  onChange: func.isRequired,
  submitButtonText: string.isRequired,
  panelUpdated: bool.isRequired,
  updateInProgress: bool.isRequired,
  errors: object.isRequired,
};

export default EditListingFeaturesPanel;
