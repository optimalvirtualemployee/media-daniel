import React from 'react';
import { FormattedMessage } from '../../util/reactIntl';

import image from './GoCampScene.svg';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';


import css from './SectionFaqsHome.css';

const SectionFaqsHome = props => {

  return (

    <div className={css.containerfaq}>
    <img className={css.faqimage} src={image} alt="footerimage" />
<div className="faqcontainer">

      <div className={css.title}>
        <h2><FormattedMessage id="SectionFaqsHome.titleLineOne" /></h2>
      </div>
      </div>

<div className="faqsection">
<Accordion allowZeroExpanded className={css.accordion}>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    How does GOCAMP work?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    How much money will I make?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    Is my property suitable to host campers?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>


        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    Do I need insurance?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>








</Accordion>
</div>
</div>

  );
};



export default SectionFaqsHome;
