import React from 'react';
import { StaticPage, TopbarContainer } from '../../containers';
import {
  LayoutSingleColumn,
  LayoutWrapperTopbar,
  LayoutWrapperMain,
  LayoutWrapperFooter,
  Footer,
} from '../../components';

import css from './FAQPage.css';
import image from './GoCampScene.svg';

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';

const FAQPage = () => {

  // prettier-ignore
  return (
    <StaticPage
      title="FAQd"
      schema={{
        '@context': 'http://schema.org',
        '@type': 'FAQPage',
        description: 'FAQs GoCamp',
        name: 'FAQs page',
      }}
    >
      <LayoutSingleColumn>
        <LayoutWrapperTopbar>
          <TopbarContainer />
        </LayoutWrapperTopbar>

        <LayoutWrapperMain className={css.staticPageWrapper}>
        <img className={css.faqimage} src={image} alt="footerimage" />
          <h1 className={css.title}>Frequently Asked Questions</h1>

        <div className={css.pageSlideContainter}>
          <h1 className={css.pageTitle}>THE GO CAMP STORY</h1>
          <p className={css.titleText}>Go Camp more than a coming Site it's an initiative to promote and revitalise the real Australia.</p>
     </div>  
          <img className={css.coverImage} src={image} alt="The go camp story." />




<div className={css.containerfaq}>
    
<div className="faqcontainer">

      <div className={css.title}>
        <h2>FAQs</h2>
      </div>
      </div>

<div className="faqsection">
<Accordion allowZeroExpanded className={css.accordion}>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    How does GOCAMP work?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    How much money will I make?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    Is my property suitable to host campers?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>


        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    Do I need insurance?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

</Accordion>
</div>
</div>





        </LayoutWrapperMain>

        <LayoutWrapperFooter>
          <Footer />
        </LayoutWrapperFooter>
      </LayoutSingleColumn>
    </StaticPage>
  );
};

export default FAQPage;

 


