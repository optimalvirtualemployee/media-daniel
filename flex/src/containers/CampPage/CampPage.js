import React from 'react';
import config from '../../config';
import { twitterPageURL } from '../../util/urlHelpers';
import { StaticPage, TopbarContainer } from '../../containers';
import {
  LayoutSingleColumn,
  LayoutWrapperTopbar,
  LayoutWrapperMain,
  LayoutWrapperFooter,
  Footer,
  ExternalLink,
  NamedLink,
} from '../../components';
import { FormattedMessage } from '../../util/reactIntl';

import css from './CampPage.css';
import image from './camp.jpg';
import imagecamp from './camp.jpg';
import imagefree1 from './Free1.jpg';
import imagefree from './Free.jpg';
import imagebest from './best.png';

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';


const CampPage = () => {
  const { siteTwitterHandle, siteFacebookPage } = config;
  const siteTwitterPage = twitterPageURL(siteTwitterHandle);

  // prettier-ignore
  return (
    <StaticPage
      title="Camp Us"
      schema={{
        '@context': 'http://schema.org',
        '@type': 'CampPage',
        description: 'Camp Saunatime',
        name: 'Camp page',
      }}
    >
      <LayoutSingleColumn>
        <LayoutWrapperTopbar>
          <TopbarContainer />
        </LayoutWrapperTopbar>

        <LayoutWrapperMain className={css.staticPageWrapper}>


      <div className={css.pageSlideContainter}>
          <h1 className={css.pageTitle}>FIND THE BEST CAMPSITES<br />ESCAPE TO A PIECE OF PARADISE.</h1>
          <p className={css.titleText}>
          Go Camp is removing the barriers,<br />
                    unlocking the gates, making the real<br />
                    Australia more available.</p>
                    <div className={css.btncenter}>
          <NamedLink className={css.btnlight}
          name="SearchPage"
          to={{
            search:
              'address=Australia&bounds=-9.04271800049806%2C159.209167991267%2C-43.8402889914172%2C112.82133800159',
          }}
        
        >
        
          <FormattedMessage id="SectionHero.browseButton" />
        </NamedLink>
        </div>
     </div>  
          <img className={css.coverImage} src={image} alt="Camp" />



      <div className={css.introcontainer}>
          <h2 className={css.introtitle}>GO CAMP IS FOR <br />THE GOOD TIMES
                </h2>
                <p>We’ve sussed out the best secret spots on private<br />
                        land, perfect for escaping the grind. Go Camp<br />
                        makes booking sites simple. No bullshit from<br />
                        caravan parks or campsites.
                        </p>

      </div>
      <div className={css.container}>
          <div className={css.row}>
              <div className={css.images} >
                    <img className={css.widthhalf} src={imagefree1} alt="Camp image" />
                    <img className={css.widthhalf} src={imagefree} alt="Camp image" />
                </div>
          </div>
      </div>

      <div className={css.sectioncard}>
      <div className={css.sectioncardtext}>
          <h2 className={css.cardtitle}> A NEW WAY <br />TO TRAVEL </h2>

                <p class={css.cardtext}>We’ve sussed out the best secret spots on private<br />
                        land, perfect for escaping the grind. Go Camp<br />
                        makes booking sites simple. No bullshit from<br />
                        caravan parks or campsites.
                        </p>
        <NamedLink name="CampPage" className={css.btndark}>
          <FormattedMessage id="SectionWhatIsGoCamp.campLink" />
        </NamedLink>

      </div>
      <img className={css.sectioncardimage} src={imagebest} alt="Camp" />
      </div>



          <div className={css.contentWrapper}>
            
            <div className={css.contentMain}>


            </div>
          </div>




<div className={css.containerfaq}>
    
<div className="faqcontainer">

      <div className={css.title}>
        <h2>FAQs</h2>
      </div>
      </div>

<div className="faqsection">
<Accordion allowZeroExpanded className={css.accordion}>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                Why use Go Camp?                
            </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                          GOCAMP connects campers with hosts of private lands 
                          to offer a unique camping experience. Signup to host 
                          or camp by creating a profile to make a booking,
                          or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    Is Go Camp in my area?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    How do I book?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>


        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
              Where does the money go?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

</Accordion>
</div>
</div>




        </LayoutWrapperMain>

        <LayoutWrapperFooter>
          <Footer />
        </LayoutWrapperFooter>
      </LayoutSingleColumn>
    </StaticPage>
  );
};

export default CampPage;
