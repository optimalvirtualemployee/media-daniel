import React from 'react';
import config from '../../config';
import { twitterPageURL } from '../../util/urlHelpers';
import { StaticPage, TopbarContainer } from '../../containers';
import {
  LayoutSingleColumn,
  LayoutWrapperTopbar,
  LayoutWrapperMain,
  LayoutWrapperFooter,
  Footer,
  ExternalLink,
  NamedLink,
} from '../../components';

import { FormattedMessage } from '../../util/reactIntl';

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';

import css from './HostPage.css';
import image from './host.jpg';
import approve from './approve.png';
import money from './make-money.png';
import list from './list.jpg';

const HostPage = () => {
  const { siteTwitterHandle, siteFacebookPage } = config;
  const siteTwitterPage = twitterPageURL(siteTwitterHandle);

  // prettier-ignore
  return (
    <StaticPage
      title="Host Us"
      schema={{
        '@context': 'http://schema.org',
        '@type': 'HostPage',
        description: 'Host Saunatime',
        name: 'Host page',
      }}
    >
      <LayoutSingleColumn>
        <LayoutWrapperTopbar>
          <TopbarContainer />
        </LayoutWrapperTopbar>

        <LayoutWrapperMain className={css.staticPageWrapper}>


      <div className={css.pageSlideContainter}>
          <h1 className={css.pageTitle}> SHARE YOU PIECE OF PARADISE <br />AND MAKE A BUCK ON THE SIDE</h1>
          <p className={css.titleText}>
              You already own the land, and there no
            Investment needed to set up your site
            Campers simple and go. You interact
            With them as much as you want.</p>
                    <div className={css.btncenter}>
          <NamedLink className={css.btndark}
          name="SearchPage"
          to={{
            search:
              'address=Australia&bounds=-9.04271800049806%2C159.209167991267%2C-43.8402889914172%2C112.82133800159',
          }}
        
        >
        
          <FormattedMessage id="SectionHero.browseButton" />
        </NamedLink>
        </div>
     </div>  
          <img className={css.coverImage} src={image} alt="Camp" />
 <div className={css.intronotice}>
<p>GO CAMP DONATES 1% FROM EVERY CAMP TO HELPING RURAL AUSTRALIANS IN NEED.</p>
</div>

<div className={css.introcontainergroup}>
<div className={css.introcontainer}>
<img className={css.sectioncardimage1} src={list} alt="Camp" />
        <h2 className={css.introtitle}>LIST YOUR PROPERTY</h2>
  <p >It’s easy to get your listing up quickly and simple to manage. You control when campers stay and how many people can camp. </p>
</div>

<div className={css.introcontainer}>
<img className={css.sectioncardimage2} src={approve} alt="Camp" />
      <h2 className={css.introtitle}>APPROVE CAMPERS</h2>
        <p>Campers can reguest to book and then you approve them , to make sure you’re comfortable.</p>
</div>

<div className={css.introcontainer}>
<img className={css.sectioncardimage3} src={money} alt="Camp" />
         <h2 className={css.introtitle}>MAKE MONEY</h2>
            <p>It’s easy and secure payment system where campers pay before they stay</p>
</div>
</div>

      <div className={css.sectioncard}>
      <div className={css.sectioncardtext}>
          <h2 className={css.cardtitle}> A NEW WAY <br />TO TRAVEL </h2>

                <p className={css.cardtext}>We’ve sussed out the best secret spots on private<br />
                        land, perfect for escaping the grind. Go Camp<br />
                        makes booking sites simple. No bullshit from<br />
                        caravan parks or campsites.
                        </p>
        <NamedLink name="CampPage" className={css.btndark}>
          <FormattedMessage id="SectionWhatIsGoCamp.campLink" />
        </NamedLink>

      </div>
      <img className={css.sectioncardimage} src={image} alt="Camp" />
      </div>



          <div className={css.contentWrapper}>
            
            <div className={css.contentMain}>


            </div>
          </div>




<div className={css.containerfaq}>
    
<div className="faqcontainer">

      <div className={css.title}>
        <h2>FAQs</h2>
      </div>
      </div>

<div className="faqsection">
<Accordion allowZeroExpanded className={css.accordion}>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                Why use Go Camp?                
            </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                          GOCAMP connects campers with hosts of private lands 
                          to offer a unique camping experience. Signup to host 
                          or camp by creating a profile to make a booking,
                          or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

<AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    Is Go Camp in my area?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
                    How do I book?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>


        <AccordionItem className={css.accordion__item}>
            <AccordionItemHeading>
                <AccordionItemButton className={css.accordion__button}>
              Where does the money go?
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={css.accordion}>
              <p className={css.faqstext}>
                        GOCAMP connects campers with hosts of private lands 
                        to offer a unique camping experience. Signup to host 
                        or camp by creating a profile to make a booking,
                        or host your land online today.
                    </p>
            </AccordionItemPanel>
        </AccordionItem>

</Accordion>
</div>
</div>




        </LayoutWrapperMain>

        <LayoutWrapperFooter>
          <Footer />
        </LayoutWrapperFooter>
      </LayoutSingleColumn>
    </StaticPage>
  );
};

export default HostPage;
