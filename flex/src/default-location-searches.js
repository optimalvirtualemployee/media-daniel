import { types as sdkTypes } from './util/sdkLoader';

const { LatLng, LatLngBounds } = sdkTypes;

// An array of locations to show in the LocationAutocompleteInput when
// the input is in focus but the user hasn't typed in any search yet.
//
// Each item in the array should be an object with a unique `id` (String) and a
// `predictionPlace` (util.types.place) properties.
export default [
  {
    id: 'default-melbourne',
    predictionPlace: {
      address: 'Melbourne, Australia',
      bounds: new LatLngBounds(new LatLng(-37.971237, 144.4926947)),
    },
  },
  {
  id: 'default-melbourne',
    predictionPlace: {
      address: 'Sydney, Australia',
      bounds: new LatLngBounds(new LatLng(-33.8479271, 150.6517972)),
    },
  },
  {
    id: 'default-queensland',
    predictionPlace: {
      address: 'Queensland, Australia',
      bounds: new LatLngBounds(new LatLng(-19.139288, 141.3296952), new LatLng(-19.139288, 141.3296952)),
    },
  },
  {
    id: 'default-adelaide',
    predictionPlace: {
      address: 'Adelaide, Australia',
      bounds: new LatLngBounds(new LatLng(-35.0004451, 138.3309827), new LatLng(-35.0004451, 138.3309827)),
    },
  },
  {
    id: 'default-perth',
    predictionPlace: {
      address: 'Perth, Australia',
      bounds: new LatLngBounds(new LatLng(-32.0397559, 115.6813574), new LatLng(-32.0397559, 115.6813574)),
    },
  },
  {
    id: 'default-tasmania',
    predictionPlace: {
      address: 'Tasmania, Australia',
      bounds: new LatLngBounds(new LatLng(-41.5708663, 143.9198363), new LatLng(-41.5708663, 143.9198363)),
    },
  },
];
